# d100 Cursed Items from Mixed Signals

This is the layout source code for a book of imaginary cursed items for Dungeons & Dragons, Pathfinder, and OSR games (such as Dungeon Raiders, Swords & Wizardry, and so on).

If you are looking for the book to download, go to [Drive Thru RPG](drivethrurpg.com).
You only need the source code here if you want to hack on the book itself (which is permitted by the book's license).
You need technical knowledge of XML, Docbook, XSL, xsltproc, Fop, and other related tools.

## Using these stylesheets

This has only been tested on Linux and BSD, but as long as you know Docbook, you should be able to use this with only modest changes to `GNUmakefile`.
      
*Assuming you're using Linux or BSD, the required software is probably available from your software repository or ports tree.*

Requirements:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to fix render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk-java](https://gitlab.com/pdftk-java/pdftk)
* [Image Magick](http://imagemagick.org/script/index.php)

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)

### Setup

The font **TeX Gyre Bonum** does not function properly because it's OTF rather than TTF.

I have converted the source OTF to TTF for you. They're included with this repository.

Place them in your ``$HOME/.local/share/fonts/t/`` directory so Docbook can detect them.


### GNUmakefile

The GNUmakefile builds a PDF, the standard delivery format for most indie RPG content. You can edit inline parameters to suit your needs. Note that the paper size is A4 by default.

Once you're ready to build:

    $ make clean pdf
    
The output is placed into a directory called ``dist``.

## License

All text is copyright by David Collins-Rivera and Seth Kenlon.
It is licensed, at your option, under the Open Game License 1.0a (OGL),
Creative Commons BY-SA,
or GNU Free Documentation License (GFDL).
